using CommunityToolkit.Mvvm.ComponentModel;
using System.Diagnostics;

namespace App;

public partial class CapturePage : ContentPage
{
    public CaptureViewModel ViewModel => (CaptureViewModel)BindingContext;
    public CapturePage(CaptureViewModel vm)
    {
        ThreadUtil.LogCurrentThead("CapturePage.CapturePage():");

        InitializeComponent();
        BindingContext = vm;
        Task.Run(DisplayImages);
    }

    public void OnCloseBtnClicked(object sender, EventArgs e)
    {
        Navigation.PopModalAsync();
    }

    private void DisplayImages()
    {
        ThreadUtil.LogCurrentThead("CapturePage.DisplayImages():");

        // Get images bytes

        var imagePrefix = "dummy-preview-";
        var imageFileNames = new List<string>();

        for (int i = 1; i <= 10; i++)
            imageFileNames.Add($"{imagePrefix}{i:D2}.jpeg");

        var imageFilePaths = imageFileNames
            .Select(Helpers.InitAndOrGetPath)
            .ToList();

        var imagesBytes = imageFilePaths
            .Select(File.ReadAllBytes)
            .ToList();

        // Hide buttons all the buttons, except [ Close ]

        ViewModel.FingerSelectorButtonVisible = false;
        ViewModel.CaptureNextButtonVisible = false;
        ViewModel.RepeatButtonVisible = false;
        ViewModel.CloseButtonVisible = true;

        // Display all the images one by one in the Image element

        ViewModel.ImageVisible = true;


        for(var j = 0; j < 10; j++)
        for(var i = 0; i < imagesBytes.Count; i++)
        {
            ViewModel.ImageBytes = imagesBytes[i];
            ViewModel.LabelText = $"Image {imageFileNames[i]}";

            Thread.Sleep(100);
        }

        // Display all the buttons

        ViewModel.FingerSelectorButtonVisible = true;
        ViewModel.CaptureNextButtonVisible = true;
        ViewModel.RepeatButtonVisible = true;
    }
}

public partial class CaptureViewModel : ObservableObject
{
    // Image
    [ObservableProperty]
    public byte[] imageBytes;

    // Section
    [ObservableProperty]
    public bool imageVisible;

    // Buttons

    [ObservableProperty]
    public bool fingerSelectorButtonVisible;

    [ObservableProperty]
    public bool captureNextButtonVisible;

    [ObservableProperty]
    public bool repeatButtonVisible;

    [ObservableProperty]
    public bool closeButtonVisible;

    [ObservableProperty]
    public string labelText;
}