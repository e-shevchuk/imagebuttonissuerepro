﻿using static System.Environment;
using static System.Environment.SpecialFolder;
using static System.IO.Path;

namespace App;

internal static class Helpers
{
    /// <summary>
    /// Checks if the file exists in the runtime destination folder. Creates it if it doesn't.
    /// </summary>
    /// <param name="resourceName">Relative path to the resource from the root/resource folder</param>
    /// <returns>Absolute path to the resource</returns>
    public static string InitAndOrGetPath(string resourceName)
    {
        var path = GetPath(resourceName);
        if (!File.Exists(path)) ReSave(resourceName);
        return path;
    }

    /// <summary>
    /// Generates the expected path to the resource file destination during the runtime.
    /// Uses the assembly location folder
    /// </summary>
    /// <param name="resourceName">Relative path to the resources folder</param>
    /// <returns>Absolute path to the resource</returns>
    static string GetPath(string resourceName)
    {
        return Combine(GetFolderPath(LocalApplicationData), resourceName);
    }

    /// <summary>
    /// Re-save to Local Application Data folder
    /// </summary>
    /// <param name="resourceName">Relative path to the resources folder</param>
    /// <returns>Absolute path to the resource</returns>
    static string ReSave(string resourceName)
    {
        // Open the resource file stream
        using var stream = FileSystem.OpenAppPackageFileAsync(resourceName).Result;

        // Read all bytes from the stream

        var ms = new MemoryStream();
        stream.CopyTo(ms);
        var bytes = ms.ToArray();

        if (bytes.Length == 0)
            throw new Exception("Could not read db file");

        // Save bytes as the same name file in specified or the Application Data folder

        var destinationPath = Combine(GetFolderPath(LocalApplicationData), resourceName);
        File.WriteAllBytes(destinationPath, bytes);

        return destinationPath;
    }
}

public static class ThreadUtil
{
    public static int UIThreadId;

    /// <summary>
    /// Throw an exception if the current thread is the UI thread
    /// </summary>
    /// <exception cref="Exception"></exception>
    public static void EnsureNotUIThread()
    {
        if (Thread.CurrentThread.ManagedThreadId == UIThreadId)
            throw new Exception("This operation cannot be performed on the UI thread");
    }

    /// <summary>
    /// Add the log record with the execution thread id and prefix info
    /// </summary>
    /// <param name="prefix"></param>
    public static void LogCurrentThead(string prefix = "")
    {
        System.Diagnostics.Debug.WriteLine(
            $"{prefix}. " +
            $"Current thread: {Thread.CurrentThread.ManagedThreadId}. " +
            $"UI thread: {UIThreadId}");
    }
}