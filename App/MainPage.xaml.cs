﻿namespace App;

public partial class MainPage : ContentPage
{
    public MainPage()
	{
		InitializeComponent();
	}

	private void OnOpenTheWindowBtnClicked(object sender, EventArgs e)
	{
        OpenCapturePage(this);
	}

    public async void OpenCapturePage(Page parentPage)
    {
        var viewModel = new CaptureViewModel();
        var page = new CapturePage(viewModel);

        await parentPage.Navigation.PushModalAsync(page);
    }
}

