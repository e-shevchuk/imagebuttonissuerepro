# Disappearing Buttons Project

This project was created to reproduce and isolate the following bug we have with MAUI & .NET 7.

## Prerequisites:

- Page (`.xaml`-based) bound to the ViewModel
  - A few buttons (`Ok`, `Cancel`, `Repeat`, `Selector`)
  - `Image`
- `CommunityToolkit.Mvvm` `[ObservableObject]`-based ViewModel
- Array of 10 slightly different images, approximately 400x400 px, represented by their `byte[]` values

## Steps to Reproduce:

1. Show/Hide buttons in a different order a few times by updating the properties of the `ViewModel` with the `[ObservableProperty]` attribute, bound to the `IsVisible` property of the corresponding buttons.
2. Hide all the buttons except `Cancel`.
3. Display all the images in the `Image` element consecutively one after another, 50 times with a delay of 100 ms (50 Image.Source updates in total within ~5 seconds).
4. Set the properties bound to `IsVisible` of the buttons to `true`.

### Expected Result:

- All the buttons are displayed.

### Actual Result:

- Only `Cancel` is visible; other buttons are not, even though `HorizontalStackLayout.HorizontalOptions` is set to `Center`, and the `Cancel` button is shifted to the side as if the other buttons occupy space on the layout but are not visible. If a user switches to another app and immediately back, or opens and closes another window, all the buttons become visible after that.